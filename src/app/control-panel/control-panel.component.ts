import { ChangeDetectionStrategy, Component, Input, Renderer2 } from '@angular/core';
import { RendererService } from '../shared/services';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ControlPanelComponent {
  @Input() selectedText: HTMLElement;

  constructor(private rendererService: RendererService) {
  }

  toggleItalic() {
    this._toggleStyle('font-style', 'italic');
  }

  toggleBold() {
    this._toggleStyle('font-weight', 'bold');
  }

  toggleUnderline() {
    this._toggleStyle('text-decoration', 'underline');
  }

  _toggleStyle(styleName: string, styleValue: string) {
    if (!this.selectedText) {
      return;
    }

    this.rendererService.toggleStyle(this.selectedText, styleName, styleValue);
  }
}
