import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Simple Text Editor';
  selectedText: HTMLElement;

  onTextSelected(selectedText: HTMLElement) {
    this.selectedText = selectedText;
  }
}
