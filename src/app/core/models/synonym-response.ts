export interface SynonymResponse {
  word: string,
  score: number
}
