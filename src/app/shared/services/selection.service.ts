import { Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable()
export class SelectionService {
  constructor(@Inject(DOCUMENT) private document: Document) {
  }

  static adjustSelection(selection: Selection, selectedRange: Range, selectedText: string) {
    const start = selection.baseOffset;
    const startNode = selection.baseNode;
    const endNode = selection.anchorNode;
    const end = start + selection.toString().trim().length;

    selectedRange.setStart(startNode, start);
    selectedRange.setEnd(endNode, end);
  }

  static removeEmptySelection(selection: Selection) {
    selection.removeAllRanges();
  }

  getSelectedRangeOrClearIfEmpty(): Range {
    const selection: Selection = this.document.getSelection();
    const selectedText = selection.toString().trim();

    if (!selectedText) {
      SelectionService.removeEmptySelection(selection);
      return null;
    }

    const selectedRange = selection.getRangeAt(0);

    SelectionService.adjustSelection(selection, selectedRange, selectedText);

    return selectedRange;
  }
}
