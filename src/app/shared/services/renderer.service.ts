import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';

@Injectable()
export class RendererService {
  private renderer: Renderer2;

  constructor(rendererFactory: RendererFactory2) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  static hasStyle(element: HTMLElement, styleName: string, styleValue: string) {
    const style = element.style[styleName];
    return style && style === styleValue;
  }

  toggleStyle(element: HTMLElement, styleName: string, styleValue: string): void {
    if (RendererService.hasStyle(element, styleName, styleValue)) {
      this.renderer.removeStyle(element, styleName);
    } else {
      this.renderer.setStyle(element, styleName, styleValue);
    }
  }

  wrapRange(range: Range, element: string = 'span'): HTMLElement {
    const wrapNode = this.renderer.createElement(element);
    range.surroundContents(wrapNode);

    return wrapNode;
  }

  replaceText(element: HTMLElement, text: string) {
    this.renderer.setProperty(element, 'innerText', text);
  }

  addAttribute(element: HTMLElement, attribute: string, value: string) {
    this.renderer.setAttribute(element, attribute, value);
  }
}
