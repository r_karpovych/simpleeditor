import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { SynonymResponse } from '../../core/models';

@Injectable()
export class SynonymsService {
  private static BASE_PATH = 'https://api.datamuse.com';

  constructor(private http: HttpClient) {
  }

  loadSynonyms(word: string): Observable<string[]> {
    return this.http
      .get(`${SynonymsService.BASE_PATH}/words?rel_syn=${word}`)
      .pipe(
        map((elements: SynonymResponse[]) => elements.map(el => el.word))
      );
  }
}
