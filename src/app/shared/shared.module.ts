import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectionService, RendererService, SynonymsService } from './services';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [],
  providers: [
    RendererService,
    SelectionService,
    SynonymsService
  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule
  ]
})
export class SharedModule {
}
