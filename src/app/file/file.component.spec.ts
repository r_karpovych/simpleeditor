import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileComponent } from './file.component';
import { TextService } from '../text-service/text.service';
import { SynonymsMenuComponent } from './synonyms-menu/synonyms-menu.component';
import { MatMenuModule } from '@angular/material';
import { SelectionService } from '../shared/services';
import { SharedModule } from '../shared/shared.module';

describe('FileComponent', () => {
  let component: FileComponent;
  let fixture: ComponentFixture<FileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatMenuModule, SharedModule],
      declarations: [FileComponent, SynonymsMenuComponent],
      providers: [TextService, SelectionService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
