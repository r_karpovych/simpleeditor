import { ChangeDetectionStrategy, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { TextService } from '../text-service/text.service';
import { RendererService, SelectionService } from '../shared/services';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FileComponent implements OnInit {
  @Output() textSelected = new EventEmitter<HTMLElement>();
  text$: Promise<string>;
  selectedText: HTMLElement;

  constructor(
    private textService: TextService,
    private selectionService: SelectionService,
    private rendererService: RendererService) {
  }

  ngOnInit() {
    this.text$ = this.textService.getMockText();
  }

  deselect() {
    this.selectedText = null;

    this.textSelected.emit(null);
  }

  selectWord(event: Event, editor: HTMLElement) {
    const selectedRange = this.selectionService.getSelectedRangeOrClearIfEmpty();

    if (!selectedRange) {
      return;
    }

    this.selectedText = event.target === editor ?
      this.rendererService.wrapRange(selectedRange) :
      event.target as HTMLElement;

    this.textSelected.emit(this.selectedText);
  }

  replaceText(synonym) {
    this.rendererService.replaceText(this.selectedText, synonym);
  }
}
