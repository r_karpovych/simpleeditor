import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SynonymsMenuComponent } from './synonyms-menu.component';
import { MatMenuModule } from '@angular/material';
import { SynonymsService } from '../../shared/services';
import { SharedModule } from '../../shared/shared.module';

describe('SynonymsMenuComponent', () => {
  let component: SynonymsMenuComponent;
  let fixture: ComponentFixture<SynonymsMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatMenuModule, SharedModule],
      providers: [SynonymsService],
      declarations: [ SynonymsMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SynonymsMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
