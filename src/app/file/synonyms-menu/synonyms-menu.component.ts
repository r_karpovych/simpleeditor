import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { SynonymsService } from '../../shared/services';
import { MatMenuTrigger } from '@angular/material';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-synonyms-menu',
  templateUrl: './synonyms-menu.component.html',
  styleUrls: ['./synonyms-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SynonymsMenuComponent implements OnChanges, OnDestroy {
  @Input() selected: HTMLElement;
  @Output() synonymSelected = new EventEmitter<string>();
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;

  synonyms: string[];
  subscription: Subscription;
  text: string;
  left: string;
  top: string;

  constructor(private synonymsService: SynonymsService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.selected) {
      return;
    }

    this.text = this.selected.innerText;
    this.subscription = this.synonymsService.loadSynonyms(this.text).subscribe((synonyms: string[]) => {
      this.synonyms = synonyms;

      if (this.trigger) {
        this.trigger.openMenu();
      }
    });

    this.alignWithSelected();
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  alignWithSelected() {
    const rect = this.selected.getBoundingClientRect();
    this.left = rect.left + 'px';
    this.top = rect.top + 'px';
  }

  selectSynonym(word) {
    this.synonymSelected.emit(word);
  }
}
